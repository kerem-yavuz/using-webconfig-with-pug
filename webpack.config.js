/** Bağımlılıklar */
const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const utils = require('./utils');
/** Module kurulumu */
module.exports = env => {
  return {
    /** Webpack ayar dosyasının ilk okuyacağı ana dosyalar */
    entry: [
      /** JS ana dosyası */
      "./src/js/master.js",
      /** Sass ana dosyası */
      "./src/sass/master.scss"
    ],
    /** JS Dosyasının çıktı alınacağı, birleştirileceği dosyanın adı ve yolu */
    output: {
      /** JS bundle adı, herhangi bir isim verilebilir */
      filename: "bundle.js",
      /** Bundle dosyasının kaydedileceği yol */
      path: path.resolve(__dirname, "public/assets/js")
    },
    /** Bazı ek dosyaları benim için seç */
    plugins: [
      new HtmlWebpackPlugin({
        template: './src/views/index.pug',
        filename: `../../index.html`,
      }),
      ...utils.pages(env)
    ],
    /** Webpack içerisinde kullanılacak olan yardımcı modüller */
    module: {
      /** Kuralları oluşturuyoruz */
      rules: [
        /** PUG dosyalarını yakala */
        {
          test: /\.pug$/,
          include: path.join(__dirname, 'src/views'),
          use: ['html-loader?attrs=false', 'pug-html-loader']
        },
        {
          /** Sonu .scss ile biten dosyaları tara */
          test: /\.scss$/,
          /** Node_module klasörünü tarama, yani içindeki hiç bir şeyi elleme */
          exclude: /node_modules/,
          /** Sass dosyalarını ve içindeki style verilerini işleyeceğin yükleyiciyi belirtiyorum */
          use: [
            "style-loader",
            {
              /** İçinde dosya yolları varsa onları da tarat */
              loader: "file-loader",
              options: {
                /** Çıktı olarak bana all.css adında bir dosya ver. İstenilen isim yazılabilir */
                name: "../css/all.css"
              }
            },
            {
              loader: 'extract-loader'
            },
            {
              /** Css içerisinde backgound-image gibi url bilgileri olan dosyaların, yollarını önemseme */
              loader: "css-loader?url=false"
            },
            {
              /** Sass dosyalarını tarat */
              loader: 'sass-loader',
              options: {
                /** Chrome gibi sass verilerini tarayıcı üzerinden kontrol edebilmem için kaynak haritası oluştur */
                sourceMap: true
              }
            }

          ]
        },
        /** JS dosyaları tarat */
        {
          test: /\.js$/,
          /** Node_module ve türevi hiç bir şeyi dahil etme */
          exclude: /(node_modules|bower_components)/,
          /** Kullanman gereken yükleyiciler */
          use: {
            loader: "babel-loader",
            options: {
              /** Async/await gibi asenkron yapıları çalıştırabilmeliyim */
              presets: ["@babel/preset-env"]
            }
          }
        },
        /** Resim ve font gibi dosyaları oku */
        {
          test: /\.(png|woff|woff2|eot|ttf|svg)$/,
          loader: "url-loader?limit=100000"
        }
      ]
    },
    /** Webpack Dev çalıştırdığımda dosyaları benim için sürekli kontrol et */
    watch: true
  }
};
