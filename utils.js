exports.pages = function (env) {
  const HtmlWebpackPlugin = require('html-webpack-plugin')
  const fs = require('fs')
  const path = require('path')
  const viewsFolder = path.resolve(__dirname, `src/views/`)

  var pages = []

  fs.readdirSync(viewsFolder).forEach(view => {
    const ext = view.split('.');
    if (ext[1] === undefined || ext[0][0] === '_')
      return false;

    const options = {
      filename: `../../${view.split('.')[0]}.html`,
      template: `src/views/${view}`,
      inject: true
    };
    pages.push(new HtmlWebpackPlugin(options));
  })
  return pages;
}